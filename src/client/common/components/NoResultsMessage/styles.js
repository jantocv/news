import styled from "@emotion/styled";
import { space } from "common/styles/spacing";
import fonts from "common/styles/fonts";

export const Message = styled.h3`
  color: red;
  font-size: ${fonts.sizes.m};
  padding: ${space.x1} 0;
`;
