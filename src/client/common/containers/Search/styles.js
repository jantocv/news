import styled from "@emotion/styled";
import { space } from "common/styles/spacing";
import fonts from "common/styles/fonts";

export const SearchScreen = styled.div`
  padding: ${space.x15};
`;

export const SearchBarWrapper = styled.div`
  display: flex;
  flex-flow: row;
  justify-content: center;
`;
