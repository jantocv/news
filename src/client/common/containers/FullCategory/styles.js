import styled from "@emotion/styled";
import { space } from "common/styles/spacing";
import screen from "common/styles/screen";

export const FullCategoryScreen = styled.div`
  padding: ${space.x15};
`;
