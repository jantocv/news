import styled from "@emotion/styled";
import { space } from "common/styles/spacing";
import fonts from "common/styles/fonts";

export const CategoryScreen = styled.div`
  padding: ${space.x15};
`;

export const CategoryScreenTitle = styled.h2`
  font-size: ${fonts.sizes.l};
`;

export const CategoryWrapper = styled.div``;

export const CategoryLabel = styled.h3`
  margin-top: ${space.x2};
  font-size: ${fonts.sizes.m};
  padding: 0 ${space.x1};
  text-transform: capitalize;
`;
export const SliderWrapper = styled.div``;
